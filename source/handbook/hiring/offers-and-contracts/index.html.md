---
layout: markdown_page
title: "Offer Packages and Contracts"
---

## On this page
{:.no_toc}

- TOC
{:toc}

## Offer Package

The employment team will create an offer package for each candidate in Greenhouse after the interview with an executive and a completed reference check.

To create the offer package, move the candidate to "Offer" in Greenhouse and select "Manage offer." Input all required and relevant information, ensuring its correctness, and submit; then click `Request Approval`. **Please note that any changes in compensation packages will result in needing re-approval from each approver.**

Note that the hiring package should include the candidate's proposed compensation in the most appropriate currency and format for their country of residence and job role. Annual and monthly salaries should be rounded up or down to the nearest whole currency unit and should always end with a zero (e.g., "50,110.00" or "23,500.00"). Hourly rates should be rounded to the nearest quarter-currency unit (e.g., 11.25/hr.).

For internal hires, be sure to include in the "Approval Notes" section the candidate's current level and position, as well as their compensation package.

You can also include any mitigating circumstances or other important details in the "Approval Notes" section of the offer details. If the comp has a variable component, please list base, on target earnings (OTE), and split in the "Approval Notes."

Please make sure that the level and position match the role page.

In case it is a public sector job family, please note (the lack of) clearances.

Information in the offer package for counter offers should include the following in the "Approval Notes" section:

   - New offer:
   - Original offer:
   - Candidate's salary expectation beginning of process:
   - Candidate's counter offer:

Anyone making comments regarding an offer should make sure to mention the recruiter and hiring manager.

The People Business Partners and People Ops Analyst will receive an email and/or Slack message notifying them of the offer. The People Business Partner for the department will either approve or deny the request, and the People Ops Analyst will ensure the compensation is in line with our compensation benchmarks. Only one approval is needed in order to move forward. Once approved, the executive of the division will then receive a notification to approve. Once approved, the CEO and Chief Culture Officer will receive a notification to approve; only one approval is required in order to move forward with the offer. Typically, the Chief Culture Officer will provide the final approval, but if the CCO is out of office, the CEO will be the final approver.

It is recommended to also ping approvers, especially the executive (and CEO if needed) in Slack with the message "Hiring approval needed for [Candidate Name] for [Position]" with a link to the candidate profile. To create the link, search for the candidate in Greenhouse, select the candidate, go to their offer details page, and copy the link. **Do not copy a link from a different section of their candidate profile.**

Once the hiring package has been approved by the approval chain, the verbal offer will be given, which will be followed by an offer email and official contract, both of which are sent through Greenhouse. If there is special compensation or bonus as part of the offer package, the CFO or Senior Director of Legal will need to approve the language before proceeding with the contract.

The Director of Recruiting will sign all contracts before they go to the candidate to sign. If the Director of Recruiting is out of office, the CCO will sign.

## Getting Offers and Contracts Ready, Reviewed, and Signed
{: #prep-contracts}

Offers made to new team members should be documented in Greenhouse through the email thread between the person authorized to make the offer and the candidate.

1. Email example is in the "Offer letter" template in Greenhouse. When using the template:
   1. make sure that you offer the correct [contract type and entity](/handbook/contracts/#how-to-use), and ask People Ops if in doubt;
   1. include both the People Ops and Recruiting aliases in the cc (this should be the default), as well as the hiring manager and executive of the role, and
   1. be sure to send the email from Greenhouse by going to the candidate profile, clicking "Email [Name]," and selecting the appropriate email template.
   1. Note: the number of proposed stock options must always be mentioned specifically, even when it is 0.
1. One person from the recruiting team will follow up with the contract. They will:
   1. Check all aspects of the offer:
      - Was it approved by everyone?
      - Do the contract type and entity make sense?
      - Is it clear how many (if any) stock options this person should receive?
      - Is all necessary information (start date, salary, location, etc.) clearly available and agreed to?
      - Does the candidate need a work permit or visa, or require an update to them before a start date can be agreed?
   1. Generates the contract within Greenhouse based on the details found in the offer package
   1. Use reply-all on the offer email to gather any missing pieces of information
   1. Ensure that, if the contract was created outside of Greenhouse, the contract has been reviewed and approved by a People Business Partner in Slack as a quality check
   1. Stage the contract in DocuSign from within Greenhouse, which emails the contract to the signing parties, with people ops, recruiting, and the hiring manager cc'd.
1. When the contract is signed, the recruiting team member should mark the candidate in Greenhouse as "Hired." Thanks to an integration between Greenhouse and BambooHR, it will automatically add an entry for the new team member in BambooHR. However, in the automatic move, "self-service" is switched off in BambooHR by default, so this must be switched on explicitly within BambooHR. The recruiting team member will ensure that the "hired" date in Greenhouse matches the date the contract was signed and close the vacancy in Greenhouse if necessary.
1. People Operations will then file the signed contract in the appropriate place and start the [onboarding issue](https://gitlab.com/gitlab-com/people-ops/employment/blob/master/.gitlab/issue_templates/onboarding.md).
1. Candidates will start the onboarding process no more than 30 days before her/his start date.

## Background Checks

Team members in the certain positions must partake in a [background check](/handbook/people-operations/code-of-conduct/#background-checks), which covers criminal and employment history.

Candidates who are in Support Engineering, Customer Success, People Ops, Finance, Sales (client-dependent), and the Executive team will be sent the link to the background check after the contract is signed. The new team member may start with GitLab prior to the returned check, but their continued employment will be contingent on a clear returned check.
