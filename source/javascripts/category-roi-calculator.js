/* global wNumb */
// Same as .on() but moves the binding to the front of the queue.
$.fn.priorityOn = function(type, selector, data, fn) {
  this.each(function() {
    var $this = $(this);

    var types = type.split(' ');

    for (var i = 0; i < types.length; i++) {
      $this.on(types[i], selector, data, fn);

      var realFn = fn || data;
      var currentBindings = $._data(this, 'events')[types[i]];
      if ($.isArray(currentBindings)) {
        for (var j = 0; j < currentBindings.length; j++) {
          if (currentBindings[j].handler.guid === realFn.guid) {
            currentBindings.unshift(currentBindings.splice(j, 1)[0]);
            break;
          }
        }
      }
    }
  });
  return this;
};

var CategoryRoiStore = {
  choices: null,
  key: 'category_roi_choices',
  setChoices: function(choices) {
    this.choices = choices;
    localStorage.setItem(this.key, JSON.stringify(choices));
  },
  getChoices: function() {
    if ( this.choices === null ) {
      this.choices = JSON.parse(localStorage.getItem(this.key)) || {};
    }
    return this.choices;
  },
  getStages: function() {
    return Object.keys(this.getChoices());
  },
  toolsForStage: function(stage) {
    return this.getChoices()[stage];
  }
};

var CategoryRoiCalculator = {
  selector: '.js-roi-calculator',
  $container: [],
  default_cost: 1000,
  tool_input_has_focus: false,
  setup_complete: false,
  dollar_format: wNumb({
    prefix: '$',
    decimals: 0,
    thousand: ','
  }),
  init: function() {
    this.$container = $(this.selector);
    if (this.$container.length > 0) {
      this.setup();
    }
  },
  setup: function() {
    this.$inputs = this.$container.find('.js-roi-calculator-input');
    this.$total = this.$container.find('#js-roi-calculator-total');
    this.$annual_total = this.$container.find('#js-roi-calculator-annual-total');
    this.$inputs.on('change', this.handleChange.bind(this));
    $(document).on('click', '.js-competitor-dropdown li', this.handleDropdownClick.bind(this));
    this.updateTotal();
    this.setDefaultCompetitors();
    this.setupAddToolSupport();
    this.addOtherToolchainOptions();
    this.preSelectChoices();
    this.setup_complete = true;
  },
  preSelectChoices: function() {
    var stages = CategoryRoiStore.getStages();
    if ( stages.length === 0 ) {
      return;
    }
    var self = this;
    // Default all pulldowns to no selection
    this.$container.find('.js-competitor-dropdown .dropdown-title').html('');
    $.each(stages, function(i, stage) {
      var keyInStage = { };
      var textInStage = { };
      self.$container.find('.js-data-row[data-stage="' + stage + '"]').each(function(j, row) {
        var $row = $(row);
        var tools = CategoryRoiStore.toolsForStage(stage);
        $.each(tools, function(k, tool) {
          // Skip row if it's the "wrong row"
          if ( tool.category_key && !$row.is('[data-category-key="' + tool.category_key + '"]') ) {
            return;
          }

          var overwrite = false;
          var focus = false;
          if (tool.key) {
            if ( keyInStage[tool.key] ) {
              return;
            }
            var $tool = $row.find('.js-roi-calculator-competitor[data-key="' + tool.key + '"]');
            if ($tool.length > 0) {
              self.updateDropdownTitle($tool, overwrite);
              keyInStage[tool.key] = true;
            }
          } else if (tool.text) {
            if ( textInStage[tool.text] ) {
              return;
            }
            var $title = $row.find('.dropdown-title');
            self.addOtherField($title, focus, tool.text, overwrite);
            textInStage[tool.text] = true;
          }
        });
      });
    });
  },
  setupAddToolSupport: function() {
    $(document).on('click', '.js-roi-calculator-add-tool', this.handleAddToolClick.bind(this));
  },
  handleAddToolClick: function(event) {
    this.addToolRow($(event.currentTarget));
  },
  getLastRow: function($rowArg) {
    var $row = $rowArg;
    var $next = $row.next();
    while ( $next.is('.js-roi-calculator-added-row') ) {
      $row = $next;
      $next = $row.next();
    }
    return $row;
  },
  addToolRow: function($$) {
    var $row = this.getLastRow($$.parents('.js-data-row:first'));
    var $newRow = $row.clone();
    $newRow.addClass('js-roi-calculator-added-row');
    $newRow.find('.data-col').filter(':not(:last-child)').html('');
    $row.find('.js-roi-calculator-add-tool').remove();
    $row.after($newRow);
    $newRow.find('.js-roi-calculator-input').val(this.default_cost);
    $newRow.find('.js-competitor-dropdown .dropdown-title').html('');
    window.setTimeout(function() { this.updateTotal(); }.bind(this), 100);
    return $newRow;
  },
  addOtherToolchainOptions: function() {
    var selector = '.js-other-tool-input';
    this.$container.find('.js-competitor-dropdown .dropdown-menu').each(function() {
      $(this).append('<li data-type="other">Other</li>');
    });
    $(document)
      .on('focusin', selector, function() {
        this.tool_input_has_focus = true;
      }.bind(this))
      .on('focusout', selector, function() {
        this.tool_input_has_focus = false;
      }.bind(this))
      .on('keypress', selector, function(event) {
        if (event.which === 13) {
          $(event.currentTarget).blur();
        }
      })
      .on('click', selector, function(event) {
        event.stopPropagation();
      })
      .on('blur', selector, this.updateStore.bind(this))
      .priorityOn('click', '[data-toggle="dropdown"]', function(event) {
        if (this.tool_input_has_focus) {
          event.stopImmediatePropagation();
        }
      }.bind(this))
    ;
  },
  setDefaultCompetitors: function() {
    var self = this;
    $('.js-competitor-dropdown ul').each(function() {
      var $$ = $(this).children(':first');
      if ( $$.length > 0 ) {
        self.updateDropdownTitle($$);
      }
    });
  },
  handleDropdownClick: function(event) {
    this.updateDropdownTitle( $(event.currentTarget) );
  },
  updateDropdownTitle: function($selected, overwriteArg) {
    var $title = $selected.parents('.dropdown').find('.dropdown-title');
    var overwrite = typeof overwriteArg === 'undefined' ? true : overwriteArg;
    if ( $selected.data('type') === 'other' ) {
      this.addOtherField($title);
    } else {
      if ( $title.html().trim() !== '' && !overwrite ) {
        var $newRow = this.addToolRow($title);
        $title = $newRow.find('.dropdown-title');
      }
      $title.data('key', $selected.data('key')).html( $selected.html() );
      this.updateStore();
    }
  },
  addOtherField: function($titleArg, focusArg, valueArg, overwriteArg) {
    var $title = $titleArg;
    var $input = $title.find('.js-other-tool-input');
    var focus = typeof focusArg === 'undefined' ? true : focusArg;
    var value = typeof valueArg === 'undefined' ? '' : valueArg;
    var overwrite = typeof overwriteArg === 'undefined' ? true : overwriteArg;
    if ( $title.html().trim() !== '' && !overwrite ) {
      // Add Row
      var $newRow = this.addToolRow($title);
      $title = $newRow.find('.dropdown-title');
      $input = [];
    }
    if ( $input.length === 0 ) {
      $input = $('<input type="text" class="js-other-tool-input other-tool-input" value="' + value + '">');
      $title.html('');
      $title.append($input);
    }
    $title.removeData('key');
    focus && $input.focus();
  },
  handleChange: function() {
    this.updateTotal();
  },
  updateTotal: function() {
    var total = this.calculateTotal();
    this.$total.text(this.dollar_format.to(total));
    this.$annual_total.text(this.dollar_format.to(total * 12));
  },
  updateStore: function() {
    if ( !this.setup_complete ) {
      return;
    }
    var choices = {};
    this.$container.find('.js-competitor-dropdown .dropdown-title').each(function() {
      var $$ = $(this);
      var $row = $$.parents('.js-data-row:first');
      var stage = $row.data('stage');
      var categoryKey = $row.data('category-key');
      if ( $$.html().trim() === '' ) {
        return;
      }

      var key = $$.data('key');
      if ( key ) {
        if (!choices[stage]) {
          choices[stage] = [];
        }
        choices[stage].push({ key: key, category_key: categoryKey });
      }

      var $otherTool = $$.find('.js-other-tool-input');
      if ( $otherTool.length > 0 ) {
        if (!choices[stage]) {
          choices[stage] = [];
        }
        choices[stage].push({ text: $otherTool.val(), category_key: categoryKey });
      }
    });
    CategoryRoiStore.setChoices(choices);
  },
  calculateTotal: function() {
    var total = 0;
    $(this.selector).find('.js-roi-calculator-input').each(function() {
      var cost = Number.parseFloat($(this).val());
      total += Number.isNaN(cost) ? 0 : cost * 100;
    });
    return total / 100;
  }
};
$(CategoryRoiCalculator.init.bind(CategoryRoiCalculator));

var CategoryRoiChooser = {
  selector: '.js-roi-choose',
  init: function() {
    if ($(this.selector).length > 0) {
      this.setup();
    }
  },
  setup: function() {
    $(document).on('click', '.js-roi-choose-tool', this.handleToolClick.bind(this));
    $(document).on('click', '.js-add-tool', this.handleAddToolClick.bind(this));
    $(document).on('click', '.js-roi-choose-report-link', this.handleReportClick.bind(this));
    $(document).on('keypress', '.js-roi-custom-tool', function(event) {
      if (event.which === 13) {
        $(event.currentTarget).blur();
      }
    });
    this.updateFromStore();
  },
  updateFromStore: function() {
    var stages = CategoryRoiStore.getStages();
    var $container = $(this.selector);
    var self = this;
    $.each(stages, function(i, stage) {
      var tools = CategoryRoiStore.toolsForStage(stage);
      $.each(tools, function(j, tool) {
        if (tool.key) {
          var $tool = $container.find('.js-roi-choose-tool[data-stage="' + stage + '"][data-competitor="' + tool.key + '"]');
          $tool.addClass('is-selected');
          if ( tool.category_key ) {
            $tool.data('category-key', tool.category_key);
          }
        } else if (tool.text) {
          var $addEl = $container.find('.js-add-tool[data-stage="' + stage + '"]');
          var focus = false;
          self.addTextInput($addEl, focus, tool.text, tool.category_key);
        }
      });
    });
  },
  handleToolClick: function(event) {
    $(event.currentTarget).toggleClass('is-selected');
  },
  handleAddToolClick: function(event) {
    this.addTextInput($(event.currentTarget));
  },
  addTextInput: function($addEl, focusArg, valueArg, categoryArg) {
    var focus = typeof focusArg === 'undefined' ? true : focusArg;
    var value = typeof valueArg === 'undefined' ? '' : valueArg;
    var categoryKey = typeof categoryArg === 'undefined' ? '' : categoryArg;
    var $input = $('<input type="text" class="custom-tool-input js-roi-custom-tool" data-stage="' + $addEl.data('stage') + '" value="' + value + '">');
    if ( categoryKey ) {
      $input.data('category-key', categoryKey);
    }
    $addEl.before($input);
    focus && $input.focus();
  },
  handleReportClick: function() {
    this.saveChoices();
  },
  saveChoices: function() {
    var choices = {};
    $(this.selector).find('.is-selected').each(function() {
      var $$ = $(this);
      if (!choices[$$.data('stage')]) {
        choices[$$.data('stage')] = [];
      }
      choices[$$.data('stage')].push({ key: $$.data('competitor'), category_key: $$.data('category-key') });
    });
    $(this.selector).find('.js-roi-custom-tool').each(function() {
      var $$ = $(this);
      if (!choices[$$.data('stage')]) {
        choices[$$.data('stage')] = [];
      }
      choices[$$.data('stage')].push({ text: $$.val(), category_key: $$.data('category-key') });
    });
    CategoryRoiStore.setChoices(choices);
  }
};
$(CategoryRoiChooser.init.bind(CategoryRoiChooser));
